const express = require('express');
const redis = require('redis');

const client = redis.createClient();
const router = express.Router();
console.log('Redis on', client.address);

/* GET users listing. */
router.get('/',
    (
        req, res) => {
      const sessionId = req.query.sessionId;
      const handle = (error, data) => {
        if (data) {
          res.status(200);
          res.json(JSON.parse(data));
        } else {
          res.status(404);
          res.end();
        }
      };
      client.get(sessionId, handle);
    }
);

router.post('/',
    (req, res) => {
      const sessionId = req.query.sessionId;
      const session = JSON.stringify(req.body);
      const handle = (error, data) => {
        res.status(200);
	res.send(data === 'OK');
      };
      client.set(sessionId,
          session,
          handle,
      );
    }
    );

module.exports = router;
